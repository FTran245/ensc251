//Use only the following libraries:
#include "parserClasses.h"
#include <string>
#include <cstring>
#include <iostream>
#include <array>

using namespace std;
//****TokenList class function definitions******
//           function implementations for append have been provided and do not need to be modified

//Creates a new token for the string input, str
//Appends this new token to the TokenList
//On return from the function, it will be the last token in the list
void TokenList::append(const string &str) {
	Token *token = new Token(str);
	append(token);
}

//Appends the token to the TokenList if not null
//On return from the function, it will be the last token in the list
void TokenList::append(Token *token) {
	if (!head) {
		head = token;
		tail = token;
	}
	else {
		tail->setNext(token);
		token->setPrev(tail);
		tail = token;
	}
}

//The token list will have a bunch of '\n' tokens due to appending and end of lines
//The show up as tokens at the end of the token list
//we should remove them
void TokenList::deletedownSpace()
{
	Token *travel = new Token; //create pointer of type Token that traverses token list

	travel = head; //create a travelling pointer to traverse the linked list

	while (travel->next != NULL)
	{
		if (travel->getStringRep() == string(1, '\n'))
		{
			Token *target = travel;
			Token *Pre = travel->getPrev();
			Token *Post = travel->getNext();

			Pre->next = Post;
			Post->prev = Pre;
			travel = Pre;
			delete target;
		}
		travel = travel->next;
	}

	if (travel->next == NULL)
	{
		if (travel->getStringRep() == string(1, '\n'))
		{
			Token *finalTarget = travel;
			Token *Last = travel->getPrev();

			Last->next = NULL;
			tail = Last;
			delete finalTarget;
		}

		else
		{
			tail = travel;
		}
	}
}

//Complete the implementation of the following member functions:
//****Tokenizer class function definitions******

//Computes a new tokenLength for the next token
//Modifies: size_t tokenLength, and bool complete
//(Optionally): may modify offset
//Does NOT modify any other member variable of Tokenizer
void Tokenizer::prepareNextToken()
{
	int search = 0;
	bool complete;
	int length = 0;

	if (!str->empty())
	{
		//find first of returns the index of what you are searching for and the length of the line you are parsing
		search = str->find_first_of("'/+-=*%;!\"#\()<>,.~[]:&|${}?_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", offset);
		length = str->length();

		if (search == -1) //you have found an unknown character not in the list^, or it's at the end of the line, more likely at end of line
		{
			//create token out of unknown character
			tokenLength = 0;
			offset = str->length();
			complete = true;
			return;
		}

		//-----------------REGULAR CHARACTERS-----------------//
		//condition: character or underscore
		if (isalpha(str->at(search)))
		{
			int ii = search;

			while (isalpha(str->at(ii)))
			{

				if (ii + 1 >= str->length())
				{
					tokenLength = tokenLength + 1;
					offset = search;
					break;
				}
				else
				{
					//check ahead for an underscore
					if (str->at(ii + 1) == '_') //if you've got an underscore in the middle of your text
					{
						ii++;
						tokenLength = tokenLength + 1;
						offset = search;
					}

					tokenLength = tokenLength + 1;
					offset = search;
					ii++;
				}
			}
		}
		
		//-----------------NUMBERS-----------------//
		if(isdigit(str->at(search)))
		{
			int dd = search;
			
			while (isdigit(str->at(dd)) && str->at(dd) != NULL)
			{
				tokenLength = tokenLength + 1; //increment for however long the number is
				offset = search;

				if (dd + 1 >= str->length())
				{
					break;
				}
				else
				{
					dd++;
				}
			}
		}

		//-----------------STRINGS "testing string" -----------------//
		//"the rabbit said 'hey where are you going?', but I didn't answer"
		
		//For ' ' 
		if(str->at(search) == '\'') //escape sequence \ lets C++ tell that you're looking for ' 
		{
			int jj = search;

			jj = str->find('\'', search + 1);
			tokenLength = (jj + 1) - search;
			offset = search;
		}

		// For " "
		if(str->at(search) == '\"')
		{
			int kk = search;
			
			kk = str->find('\"', search + 1);
			tokenLength = (kk + 1) - search;
			offset = search;
		}


		//-----------------BOOLEAN OPERATORS-----------------//
		//----------------- < -----------------//
		if (str->at(search) == '<')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}

			else
			{
				//For <= 
				if (str->at(search + 1) == '=')
				{
					offset = search;
					tokenLength = 2;
				}

				//For left shift >>
				else if (str->at(search + 1) == '<')
				{
					offset = search;
					tokenLength = 2;
				}

				//if next is not another =, then you've got < 
				else if (str->at(search + 1) != '=')
				{
					offset = search;
					tokenLength = 1;
				}
			}
		}

		//----------------- > -----------------//
		if (str->at(search) == '>')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}

			else 
			{
				//For >= 
				if (str->at(search + 1) == '=')
				{
					offset = search;
					tokenLength = 2;
				}

				//For right shift >>
				else if (str->at(search + 1) == '>')
				{
					offset = search;
					tokenLength = 2;
				}

				//if next is not another =, then you've got > 
				else if (str->at(search + 1) != '=')
				{
					offset = search;
					tokenLength = 1;
				}
			}
		}

		//----------------- = -----------------//
		if (str->at(search) == '=')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}
			else
			{
				//For  ==
				if (str->at(search + 1) == '=')
				{
					offset = search;
					tokenLength = 2;
				}

				//if next is not another =, then you've got =
				else if (str->at(search + 1) != '=')
				{
					offset = search;
					tokenLength = 1;
				}
			}
		}

		//----------------- ! -----------------//
		if (str->at(search) == '!')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}
			else
			{
				//----------------- For != -----------------//
				if (str->at(search + 1) == '=')
				{
					tokenLength = 2;
					offset = search;
				}

				// For regular !
				else
				{
					tokenLength = 1;
					offset = search;
				}
			}
		}

		//----------------- | -----------------//
		if (str->at(search) == '|')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}

			else
			{
				//-----------------For || operator -----------------//
				if (str->at(search + 1) == '|')
				{
					tokenLength = 2;
					offset = search;
				}

				else
				{
					tokenLength = 1;
					offset = search;
				}
			}
			
		}

		//----------------- & -----------------//
		if (str->at(search) == '&')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}
			else
			{
				//-----------------For && operator -----------------//
				if (str->at(search + 1) == '&')
				{
					tokenLength = 2;
					offset = search;
				}

				//For regular &
				else
				{
					tokenLength = 1;
					offset = search;
				}
			}
			
		}

		//-----------------ARITHMETIC OPERATORS-----------------//
		//----------------- // -----------------//
		//----------------- + -----------------// add
		if(str->at(search) == '+')
		{
			tokenLength = 1;
			offset = search;
		}

		//----------------- - -----------------// subtract
		if(str->at(search) == '-')
		{
			// VHDL comment starts with --
			if (str->at(search + 1) == '-')
			{
				tokenLength = 2;
				offset = search;
			}

			else
			{
				tokenLength = 1;
				offset = search;
			}
		}

		//----------------- * -----------------//multiply
		if(str->at(search) == '*')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}
			else
			{
				//----------------- for ** -----------------// apparently its the exponent operator in VHDL
				if (str->at(search + 1) == '*')
				{
					tokenLength = 2;
					offset = search;
				}
				else
				{
					tokenLength = 1;
					offset = search;
				}
			}
		}

		//----------------- ^ -----------------//regular exponent
		if (str->at(search) == '^')
		{
			tokenLength = 1;
			offset = search;
		}

		//----------------- \ or / -----------------//divide
		if (str->at(search) == '/' || str->at(search) == '\\')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------PUNCTUATION-----------------//

		//-----------------COLONS : -----------------//
		if(str->at(search) == ':')
		{
			if (search + 1 >= length)
			{
				offset = search;
				tokenLength = 1;
			}
			else
			{
				//----------------- For := variable assignment 
				if (str->at(search + 1) == '=')
				{
					tokenLength = 2;
					offset = search;
				}
				else
				{
					tokenLength = 1;
					offset = search;
				}
			}
		}

		//-----------------COMMAS , -----------------//
		if(str->at(search) == ',')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------SEMICOLONS ; -----------------//
		if(str->at(search) == ';')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------PERIODS . -----------------//
		if(str->at(search) == '.')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------BRACKETS { }, ( ), [ ] -----------------//
		if(str->at(search) == '{')
		{
			tokenLength = 1;
			offset = search;
		}

		if(str->at(search) == '}')
		{
			tokenLength = 1;
			offset = search;
		}

		if(str->at(search) == '(')
		{
			tokenLength = 1;
			offset = search;
		}

		if(str->at(search) == ')')
		{
			tokenLength = 1;
			offset = search;
		}

		if(str->at(search) == '[')
		{
			tokenLength = 1;
			offset = search;
		}

		if(str->at(search) == ']')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------MISCELLANEOUS -----------------//

		//-----------------For $ -----------------//
		if (str->at(search) == '$')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------For  #-----------------//
		if (str->at(search) == '#')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------For  ?-----------------//
		if (str->at(search) == '?')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------For  ~-----------------//
		if (str->at(search) == '~')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------For  %-----------------//
		if (str->at(search) == '%')
		{
			tokenLength = 1;
			offset = search;
		}

		//-----------------For  `-----------------//
		if (str->at(search) == '`')
		{
			tokenLength = 1;
			offset = search;
		}
	}

	complete = isComplete();
}

//Sets the current string to be tokenized
//Resets all Tokenizer state variables
//Calls Tokenizer::prepareNextToken() as the last statement before returning.
void Tokenizer::setString(string *Str) 
{
	//reset variables
	complete = false;
	offset = 0;
	tokenLength = 0;
	str = Str; //set the string of Tokenizer to the input from the file
	prepareNextToken(); //call to prep next token
}

//Returns the next token. Hint: consider the substr function
//Updates the tokenizer state
//Updates offset, resets tokenLength, updates processingABC member variables
//Calls Tokenizer::prepareNextToken() as the last statement before returning.
string Tokenizer::getNextToken() 
{
	string next = "\0";

	//substr creates a smaller string with the indices you provide
	next = str->substr(offset, tokenLength);
	offset += tokenLength; //need to update the offset, tells where you are in the current line
	
	tokenLength = 0; //reset the tokenLength to count the length of the next token
	
	prepareNextToken();
	return next;
}



//****Challenge Task Functions******

//Removes the token from the linked list if it is not null
//Deletes the token
//On return from function, head, tail and the prev and next Tokens (in relation to the provided token) may be modified.
void TokenList::deleteToken(Token *token) 
{
	Token *setPrev = new Token;
	Token *setNext = new Token;

	setPrev = token->getPrev();
	setNext = token->getNext();

	if (setPrev == NULL)
	{
		setNext->prev = NULL;
		head = setNext;
		delete token;
		return;
	}

	else if (setNext == NULL)
	{
		setPrev->next = NULL;
		tail = setPrev;
		delete token;
		return;
	}

	else
	{
		//set the doubly linked list excluding the targeted token
		setPrev->next = setNext;
		setNext->prev = setPrev;
		delete token;
	}
}

//Removes all comments from the tokenList including the -- marker
//Returns the number of comments removed
int removeComments(TokenList &tokenList) 
{
	Token *travel = new Token;
	bool flag = 0;
	string str;
	int commentsRemoved = 0;

	travel = tokenList.getFirst();

	while (travel != NULL)
	{
		str = "--";
		if (travel->getStringRep() == str)
		{
			Token *del = travel;

			commentsRemoved++; //increment number of comments removed when you find --

			while (del->getStringRep() != string(1, '\n'))
			{
				Token *target = del;
				del = del->getNext();
				tokenList.deleteToken(target);
			}
			travel = del;
		}

		travel = travel->getNext();

	}

	return commentsRemoved;
}


void TokenList::findAndSetTokenDetails(Token *token)
{

	string str;

	//declare tables of special words in VHDL
	const string tableOfKeyWords[] = { "abs", "access", "after", "alias", "all", "array", "assert", "attriute", "begin", "block", "body", "buffer", "bus", "case", "component", "configuration", "constant", "disconnect", "downto", "else", "elsif", "end", "entity", "exit", "file", "for", "function", "generate", "generic", "group", "guarded", "if", "impure", "in", "inertial", "inout", "is", "label", "library", "linkage", "literal", "loop", "map", "mod", "new", "next", "null", "of", "on", "open", "or", "others", "out", "package", "port", "postponed", "procedure", "process", "pure", "range", "record", "register", "reject", "rem", "report", "return", "rol", "ror", "select", "severity", "shared", "sla", "sll", "sra", "srl", "subtype", "then", "to", "transport", "type", "unaffected", "units", "until", "use", "variable", "wait", "when", "while", "with", "and", "or", "xor", "nand", "nor", "xnor", "not", "true", "false", "entity", "architecture", "signal" };

	const string tableOfIdentifiers[] = { "signal", "std_logic", "std_logic_vector" };

	const string tableOfPunctuators[] = { ";", ":", "--", ",", "."};

	const string tableOfOperators[] = { "and", "or", "xor", "nand", "nor", "xnor", "not", "+", "-", "/", "*", "<=", "<", ">", "++", "--", "<<", ">>", "!=", "&", "&&", "|", "||", "[", "]", "==" };

	int countK = 0;
	int countO = 0;
	int countI = 0;
	int countP = 0;

	//include library <array> for this to work
	int sizeK = sizeof(tableOfKeyWords);
	int sizeI = sizeof(tableOfIdentifiers);
	int sizeP = sizeof(tableOfPunctuators);
	int sizeO = sizeof(tableOfOperators);

	str = token->getStringRep(); //get the value of the string
	
	//compare the string value to all the tables, then set boolean values
	for (countK = 0; countK < sizeK; countK++)
	{
		if (str == tableOfKeyWords[countK])
		{
			token->isKeyword = true; //set token type to keyword
		}
	}

	for (countI = 0; countI < sizeI; countI++)
	{
		if (str == tableOfKeyWords[countI])
		{
			token->isIdentifier = true; //set token type to keyword
		}
	}

	for (countP = 0; countP < sizeP; countP++)
	{
		if (str == tableOfKeyWords[countP])
		{
			token->isOther = true; //set token type to keyword
		}
	}

	for (countO = 0; countO < sizeO; countO++)
	{
		if (str == tableOfKeyWords[countO])
		{
			token->isOperator = true; //set token type to keyword
		}
	}
	
	cout << "testing";
}

